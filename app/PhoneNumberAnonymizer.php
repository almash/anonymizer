<?php

/**
 * Class PhoneNumberAnonymizer
 *
 * Sample usages (parametrized):
 *
 * +48 666 777 888 -> +48 666 777 XXX
 * +246 666 777 888 -> +48 666 7XX XXX
 * +246 666 777 888 -> +48 666 777 88*
 * +246 666 777 888 -> +48 xxx xxx xxx
 */
class PhoneNumberAnonymizer
{
    /**
     * @var string
     */
    private $replacement;

    /**
     * @var int
     */
    private $lastDigits;

    /**
     * PhoneNumberAnonymizer constructor.
     * @param string $replacement
     * @param int $lastDigits
     */
    public function __construct($replacement, $lastDigits = 3)
    {
        $this->replacement = $replacement;
        $this->lastDigits = $lastDigits;
    }

    /**
     * @param string $text
     * @return array of string
     */
    public function anonymize($text)
    {
        $pattern = '/(?! )([0-9 ]+)(?<! )/i';
        preg_match_all($pattern, $text, $matches);
        $count = count($matches[0]);
        $text_arr = $this->explodeX($text, $matches[0]);
        $rep_text = '';
        for($i = 0; $i < $count ; $i++){
            $matches[0][$i] = $this->replaceX($matches[0][$i]);
            $rep_text .= $text_arr[$i].$matches[0][$i];
        }
        $rep_text .= $text_arr[$i];

        $text = $rep_text;
        return $text;
    }

    /**
     * @param $txt
     * @param $matches
     * @return array
     */
    private function explodeX($txt, $matches){
        $arr = array();
        $i =0;
        foreach ($matches as $match){
            $exp = explode($match, $txt);
            $arr[$i] = $exp[0];
            $txt = $exp[1];
            $i++;
        }
        $arr[$i] = $txt;

        return $arr;
    }

    /**
     * @param $tel
     * @return mixed
     */
    private function replaceX($tel){
        $count = strlen($tel);
        $last_digit = $this->lastDigits;
        for($i = $count -1; $i >= 0 && $last_digit > 0; $i--){
            if( $tel[$i] != ' ') {
                $tel[$i] = $this->replacement;
                $last_digit--;
            }
        }

        return $tel;
    }
}